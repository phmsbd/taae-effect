//
//  ViewController.h
//  TAAE-Effect
//
//  Created by Patrick Madden on 6/4/16.
//  Copyright © 2016 Secret Base Design. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Universe.h"

@interface ViewController : UIViewController<AEAudioReceiver>
@property (nonatomic, strong) Universe *universe;
@property (nonatomic, strong) IBOutlet UISlider *volumeSlider;
@property (nonatomic, retain) AEBlockChannel *effectGenerator;

@end

