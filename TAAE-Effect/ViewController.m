//
//  ViewController.m
//  TAAE-Effect
//
//  Created by Patrick Madden on 6/4/16.
//  Copyright © 2016 Secret Base Design. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize universe;
@synthesize volumeSlider;
@synthesize effectGenerator;

static void audioCallback(id THIS, AEAudioController *audioController, void *source, const AudioTimeStamp *time, UInt32 frames, AudioBufferList *audio);
void iaaChanged(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement);

static float volume;

// Major hack.  Just grab some static buffer space to hold the incoming
// audio.  We write here in the audioReceiver, and pull out of it in the
// effect generator.  Ideally, this is a circular buffer, but I'm punting
// to get a proof-of-concept app together.
static Float32 bufferLeft[4096];
static Float32 bufferRight[4096];
- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

	volume = 1;
	
	universe = [[Universe alloc] init];
	[[universe audioController] addInputReceiver:self];
	// AudioSessionAddPropertyListener(kAudioSessionProperty_AudioRouteChange, uiAudioSessionPropertyListener, (__bridge void *)self);
	
	AudioUnitAddPropertyListener([[universe audioController] audioUnit], kAudioUnitProperty_IsInterAppConnected,
								 iaaChanged, (__bridge void * _Nullable)(self));
	
	AudioComponentDescription desc = { kAudioUnitType_RemoteEffect, 'sbfx', 'sbda', 0, 0 };
	AudioOutputUnitPublish(&desc, CFSTR("TAAE Effect"), 0, [[universe audioController] audioUnit]);
	
	effectGenerator = [AEBlockChannel channelWithBlock:^(const AudioTimeStamp  *time,
														 UInt32           frames,
														 AudioBufferList *audio)
	{
		static int count = 0;
		Float32 *left, *right;
		float total = 0;
		left = (Float32 *)audio->mBuffers[0].mData;
		right = (Float32 *)audio->mBuffers[1].mData;

		for ( int i=0; i<frames; i++ )
		{
			left[i] = bufferLeft[i] * volume;
			right[i] = bufferRight[i] * volume;
			
			total += fabs(left[i]);
		}
		if ((count % 100) == 0)
		{
			NSLog(@"Output process %d %f", count, total);
		}
		++count;
	}];

	effectGenerator.audioDescription = [AEAudioController nonInterleavedFloatStereoAudioDescription];
	[[universe audioController] addChannels:[NSArray arrayWithObjects:effectGenerator, nil]];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


-(AEAudioControllerAudioCallback)receiverCallback {
	return &audioCallback;
}


static void audioCallback(id THISptr, AEAudioController *audioController, void *source, const AudioTimeStamp *time, UInt32 frames, AudioBufferList *audio)
{
	static int count = 0;
	float total = 0;
	Float32 *leftPtr = audio->mBuffers[0].mData;
	Float32 *rightPtr = audio->mBuffers[1].mData;

	for (int i = 0; i < frames; ++i)
	{
		bufferLeft[i] = leftPtr[i];
		bufferRight[i] = rightPtr[i];
		total += fabs(leftPtr[i]);
	}
	if ((count % 100) == 0)
	{
		NSLog(@"Input process %d  %f", count, total);
	}
	++count;
}


// Callback for detecting changes to the Inter-App connection chain.  When inserted or removed, you need to stop, and
// then start the audio graph.  Why?  Because, that's why!
void iaaChanged(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement)
{
	ViewController *SELF = (__bridge ViewController *)inRefCon;
	
	NSLog(@"IAA change callback.");
	NSError *err;
	[[[SELF universe] audioController] stop];
	[[[SELF universe] audioController] start:&err];
}


-(IBAction)sliderMoved:(id)sender
{
	volume = [volumeSlider value];
	// NSLog(@"Volume set to %f", volume);
}

-(IBAction)restartAudio:(id)sender
{
	NSError *err;
	
	[[universe audioController] stop];
	[[universe audioController] start:&err];
}

@end
